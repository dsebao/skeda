/*
AJAX - Send data using ajax
Receive data, use one callback for beforesend and another for handle output
*/


function ajaxSend(mydata,beforeSend,handleData,type = 'POST',datatype = 'json',globaltype = true){
    $.ajax({
        type: type,
        language: 'es_ES',
        url: jsvar.ajaxurl,
        dataType: datatype,
        data: mydata,
        global: globaltype,
        beforeSend: function (data) {
            beforeSend(data);
        },
        success: function(data){
            console.log(data);
            handleData(data);
        },
        error: function(e){
            console.log(e);
            $.notify(e,{type: 'danger'});
        }
    });
}

function nav(){
    $('.navhoverdropdown').bootnavbar();
    var navm = $('#dash-menu');
    var url = navm.data('page');
    navm.find('a').each(function(){
    	var d = $(this);
		var href = d.attr('href');
		if (href === url)
			d.parent().addClass('active');
		else
			d.parent().removeClass('active');
	});
}


function filterSubdomain(){
    $("#urlsubdomain").on("keypress", function (event) {

        var englishAlphabetAndWhiteSpace = /[A-Za-z ]/g;
        var key = String.fromCharCode(event.which);

        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
            return true;
        }

        return false;
    });

    $('#urlsubdomain').on("paste", function (e) {
        e.preventDefault();
    });
}



function ajaxForms(){
    var nonce = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': nonce}});

    $('form.s_form').validator().submit(function(event) {
        var form = $(this);

        if (event.isDefaultPrevented()) {
        } else{
            event.preventDefault();
            ajaxSend(form.serialize() + '&_wpn=' + jsvar.ajax_nonce,function(data){
                //Before send
            },function(out){
                if(out.action){
                    var a = out.message;
                    form.find('.message').html('<div class="alert alert-'+out.type+'">'+a+'</div>');
                }
                if(out.resetform)
                    form.trigger('reset');
            });
        }
    });
}

function UI(){
    $('.avoid').keyup(function() {
        $(this).val(this.value.toLowerCase().replace(/\s/g, ''));
    });
}

function forms(){
    $('form.multistep').on('click', 'button.tostep', function(e) {
        var form = $(this).closest('form');
        event.preventDefault();
        form.validator('validate');
        var step = $(this).data('step');
        var to = $(this).data('tostep');
        var hasErrors = form.has('.has-error:visible').length;
        if(hasErrors != 0){
        } else {
            form.validator('destroy');
            if(to == '#submit'){
                form.submit();
            } else {
                form.find(step).hide();
                form.find(to).show();
            }
        }
    });
}

jQuery(document).ready(function($) {

	nav();

	ajaxForms();

    forms();

    UI();

    filterSubdomain();
});