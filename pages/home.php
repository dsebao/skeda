<?php

/*
Template Name: Home
*/

get_header();?>
<section id="mainfold" class="fdb-block bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="wow fadeInLeft"><?php _e('Online-booking and business management','skeda');?></h1>
                <p class="wow fadeIn lead" data-wow-delay="0.2s"><?php _e('Skēda — is fast and simple online booking for your clients. Convenient and free CRM-platform for your business.','skeda');?></p>
                <p class="lead wow fadeIn" data-wow-delay="0.4s"><?php _e('Thanks to Skēda everyday matters become easy and pleasant. And your clients feel happier.','skeda');?></p>
                <p class="wow fadeInDown" data-wow-delay="0.6s">
                    <a href="<?php bloginfo('url');?>/signup" class="btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/login" class="btn btn-secondary"><?php _e('Sign in','skeda');?></a>
                </p>
            </div>
            <div class="col-12 col-md-6 m-auto ml-lg-auto mr-lg-0 pt-5 pt-lg-0">
                <img alt="image" class="img-fluid wow fadeInRight" src="<?php bloginfo('template_url');?>/src/images/organize.svg">
            </div>
        </div>
    </div>
</section>

<section class="fdb-block">
    <div class="container">
        <div class="row">
            <div class="col-12 text-left">
                <h1 class=""><?php _e('Skēda is suitable for many business types','skeda');?></h1>
                <p class="lead"><?php _e('Regardless of the number of employees, number of offices and bookings  — Skēda is an excellent fit for you.','skeda');?></p>
            </div>
        </div>
        <div class="row text-left mt-5">
            <div class="col-md-4">
                <div class="imagethumb p-4">
                    <img alt="image" class="img-fluid rounded" src="<?php bloginfo('template_url');?>/src/images/fitness.svg">
                </div>
                <a href="<?php bloginfo('url');?>/features" class="btn btn-primary btn-block text-white p-3 my-4 text-center"><?php _e('Fitness','skeda');?></a>
            </div>
            <div class="col-md-4">
                <div class="imagethumb p-4">
                    <img alt="image" class="img-fluid rounded" src="<?php bloginfo('template_url');?>/src/images/beauty.svg">
                </div>
                <a href="<?php bloginfo('url');?>/features" class="btn btn-primary btn-block text-white p-3 my-4 text-center"><?php _e('Beauty salons','skeda');?></a>
            </div>
            <div class="col-md-4">
                <div class="imagethumb p-4">
                    <img alt="image" class="img-fluid rounded" src="<?php bloginfo('template_url');?>/src/images/hair.svg">
                </div>
                <a href="<?php bloginfo('url');?>/features" class="btn btn-primary btn-block text-white p-3 my-4 text-center"><?php _e('Barber shops','skeda');?></a>
            </div>
        </div>
    </div>
</section>

<section class="fdb-block fp-active">
    <div class="container">
        <div class="row justify-content-center pb-5">
            <div class="col-12 text-center">
                <h1 class=""><?php _e('Useful and convenient features','skeda');?></h1>
            </div>
        </div>
        <div class="row text-left align-items-center pt-5 pb-md-5">
            <div class="col-12 col-md-5">
                <img alt="image" class="img-fluid" src="<?php bloginfo('template_url');?>/src/images/calendar.svg">
            </div>
            <div class="col-12 col-md-5 m-md-auto pb-md-5">
                <h2><strong><?php _e('Calendar','skeda');?></strong></h2>
                <p class="lead"><?php _e('Skēda has all the needed features for comfortable overview of the timetable. You can easily create, postpone, delete bookings.  Never had client management been so simple.','skeda');?></p>
                <p>
                    <a href="<?php bloginfo('url');?>/signup" class="btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/features" class="btn btn-secondary"><?php _e('Learn more','skeda');?></a>
                </p>
            </div>
        </div>
        <div class="row text-left align-items-center pt-5 pb-md-5">
            <div class="col-12 col-md-5 m-md-auto order-md-5">
                <img alt="image" class="img-fluid" src="<?php bloginfo('template_url');?>/src/images/booking.svg">
            </div>
            <div class="col-12 col-md-5">
                <h2><strong><?php _e('Online-booking','skeda');?></strong></h2>
                <p class="lead"><?php _e('With Skēda you can make clients happier: no more response delays and waiting.  Be ready to receive more bookings, acquire new customers and boost your business.','skeda');?></p>
                <p>
                    <a href="<?php bloginfo('url');?>/signup" class="btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/features" class="btn btn-secondary"><?php _e('Learn more','skeda');?></a>
                </p>
            </div>
        </div>
        <div class="row text-left align-items-center pt-5 pb-md-5">
            <div class="col-12 col-md-5">
                <img alt="image" class="img-fluid" src="<?php bloginfo('template_url');?>/src/images/data.svg">
            </div>
            <div class="col-12 col-md-5 m-md-auto">
                <h2><strong><?php _e('Statistics','skeda');?></strong></h2>
                <p class="lead"><?php _e('You don’t need anymore to manually keep track of key indicatiors. In Skēda there are already all the needed instruments: it will automatically create graphs and charts and conduct calculations.','skeda');?></p>
                <p>
                    <a href="<?php bloginfo('url');?>/signup" class="btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/features" class="btn btn-secondary"><?php _e('Learn more','skeda');?></a>
                </p>
            </div>
        </div>
        <div class="row text-left align-items-center pt-5 pb-md-5">
            <div class="col-12 col-md-5 m-md-auto order-md-5">
                <img alt="image" class="img-fluid mb-5" src="<?php bloginfo('template_url');?>/src/images/sync.svg">
            </div>
            <div class="col-12 col-md-5">
                <h2><strong><?php _e('Customizable site','skeda');?></strong></h2>
                <p class="lead"><?php _e('Skēda will automatically create a site for you. The setup takes just minutes. It is customizable, so you can add all necessary information. Share the link to your site, so your clients can book online!','skeda');?></p>
                <p>
                    <a href="<?php bloginfo('url');?>/signup" class="btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/features" class="btn btn-secondary"><?php _e('Learn more','skeda');?></a>
                </p>
            </div>
        </div>
    </div>
</section>

<section class="fdb-block">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 col-xl-5">
                <h1 class=""><?php _e('And many more…','skeda');?></h1>
                <p class="lead"><?php _e('You can start using Skēda right now or learn more about the features and opportunities that Skēda provides.','skeda');?></p>
                <h3 class="font-weight-bold d-block my-5"><?php _e('Join Skēda, the most convenient CRM system.','skeda');?></h3>
                <p>
                    <a href="<?php bloginfo('url');?>/signup" class="btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/features" class="btn btn-secondary"><?php _e('Learn more','skeda');?></a>
                </p>
            </div>
            <div class="col-12 col-md-5 m-md-auto order-md-5">
                <img alt="image" class="img-fluid my-5" src="<?php bloginfo('template_url');?>/src/images/more.svg">
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>