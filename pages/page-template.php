<?php

/*
Template Name: Page Empty
*/

get_header();

if (have_posts()) : while (have_posts()) : the_post();?>

<section class="fdb-block heropage">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-8 mx-auto text-center">
                <h1 class="wow fadeInLeft"><?php the_title();?></h1>
            </div>
        </div>
    </div>
</section>

<div id="containerbuilder">
	<?php the_content();?>
</div>
<?php endwhile; endif;?>
<?php get_footer();?>