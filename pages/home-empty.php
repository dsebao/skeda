<?php

/*
Template Name: Home Empty
*/

get_header();?>
<section id="mainfold" class="fdb-block">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <h1 class="wow fadeInLeft"><?php _e('Online-booking and business management','skeda');?></h1>
                <p class="wow fadeIn lead " data-wow-delay="0.2s"><?php _e('Skēda — is fast and simple online booking for your clients. Convenient and free CRM-platform for your business.','skeda');?></p>
                <p class="lead wow fadeIn" data-wow-delay="0.4s"><?php _e('Thanks to Skēda everyday matters become easy and pleasant. And your clients feel happier.','skeda');?></p>
                <p class="wow fadeIn pt-3 mb-0" data-wow-delay="0.6s">
                    <a href="<?php bloginfo('url');?>/signup" class="d-md-inline-block d-block mb-4 btn btn-primary"><?php _e('Start using','skeda');?></a>
                    <a href="<?php bloginfo('url');?>/login" class="d-md-inline-block d-block mb-4 btn btn-secondary"><?php _e('Sign in','skeda');?></a>
                </p>
            </div>
            <div class="col-12 col-md-6 m-auto ml-lg-auto mr-lg-0 pt-5 pt-lg-0">
                <img alt="image" class="img-fluid wow fadeInRight" src="<?php bloginfo('template_url');?>/src/images/organize.svg">
            </div>
        </div>
    </div>
</section>

<?php the_content();?>

<?php get_footer();?>