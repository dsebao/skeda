<?php

/*
Template Name: Signup
*/

if(is_user_logged_in()){

}

//Globals vars for receive the state of the difeerents requests
global $waitingvalidation, $message, $confirmedemail,$sitecreated;

get_header();
?>
<main class="mt-5" id="loginpage">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-6 mx-md-auto my-md-5">
				<?php

				/*
				Notification to Validate the user
				*/

				if(isset($_GET['action']) && $_GET['action'] == 'validatemail'){?>

				<div class="mb-7">
					<h1 class="h2 font-weight-normal mb-3"><?php _e('One more step','skeda');?></h1>
				</div>
				<p class="lead font-weight-light">
					<?php _e('We have sent you a message. Please check your inbox to confirm your email','skeda');?>
				</p>

				<?php
				/*
				If user has submited your basic info adn validated their email, create the site with the business info
				*/
				} elseif(isset($confirmedemail) && $confirmedemail){?>
				<form id="signup" class="mb-5" data-toggle="validator" action="" method="post">
					<div class="mb-7">
						<h1 class="h2 font-weight-normal mb-3"><?php _e('Lets begin!','skeda');?></h1>
					</div>

					<div class="message">
						<?php echo (isset($message)) ? $message : "";?>
					</div>

					<p class="lead font-weight-light">
						<?php _e('Please complete the info of your site','skeda');?>
					</p>

					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="business" id="business" placeholder="<?php _e('Name of the business','skeda');?>" aria-label="<?php _e('Name of the','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						<select name="businesstype" id="businesstype" class="form-control custom-select" style="width:100%" required>
							<option value=""><?php _e('Business type','skeda');?></option>
							<?php getOptionsCustom($GLOBALS['BUSINESS_TYPE']);?>
						</select>
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						<select name="position" id="position" class="form-control custom-select" style="width:100%" required>
							<option value=""><?php _e('Your position','skeda');?></option>
							<?php getOptionsCustom($GLOBALS['BUSINESS_POSITION']);?>
						</select>
						<div class="help-block with-errors"></div>
					</div>

					<div class="input-group mb-3">
						<input id="urlsubdomain" type="text" name="url" pattern="^[_A-z0-9]{1,}$" maxlength="30" class="form-control avoid" placeholder="<?php _e('misite','skeda');?>" aria-describedby="url-addon">
						<div class="input-group-append">
							<span class="input-group-text" id="url-addon">.skeda.ru</span>
						</div>
					</div>

					<p class="text-right my-3">
						<?php wp_nonce_field('createsite', '_wpnonce');?>
						<input type="hidden" name="userid" value="<?php echo $_GET['user'];?>">
						<button type="submit" class="btn btn-block btn-primary btn-block"><?php _e('Create','skeda');?></button>
					</p>
				</form>

				<?php

				/*
				If user has created the site successfully, show the Confirm page and a button to sign in
				*/

				} elseif(isset($_GET['action']) && $_GET['action'] == 'sitecreated'){?>

				<div class="mb-7">
					<h1 class="h2 font-weight-normal mb-3"><?php _e('Your site is ready!','skeda');?></h1>
				</div>
				<p class="lead font-weight-light">
					<?php _e('We are redirecting you to the login page. Please wait a few seconds.','skeda');?>
				</p>
				<p class="mb-5">
					<a href="<?php echo get_admin_url($_GET['id']); ?>" class="btn btn-primary"><?php _e('Sign in','skeda');?></a>
				</p>

				<script>
					window.setTimeout(function() {
						location.href = "<?php echo get_admin_url($_GET['id']);?>"
					}, 4000);
				</script>

				<?php
				/*
				First screen to signup the user
				*/
				} else{?>

				<form id="signup" class="mb-5" data-toggle="validator" action="" method="post">
					<div class="mb-7">
						<h1 class="h2 font-weight-normal mb-3"><?php _e('Welcome!','skeda');?></h1>
					</div>

					<div class="message">
						<?php echo (isset($message)) ? $message : "";?>
					</div>

					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="personname" id="personname" placeholder="<?php _e('Name','skeda');?>" aria-label="<?php _e('Name','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="personlastname" id="personlastname" placeholder="<?php _e('Surname','skeda');?>" aria-label="<?php _e('Suurname','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						<input type="email" class="form-control" name="email" id="email" placeholder="<?php _e('Email','skeda');?>" aria-label="<?php _e('Email','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="phone" id="phone" placeholder="<?php _e('Your phone number','skeda');?>" aria-label="<?php _e('Your phone number','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>


					<div class="form-group has-feedback">
						<input type="password" class="form-control" data-minlength="6" name="pass" id="pass" placeholder="<?php _e('Create Password','skeda');?>" aria-label="<?php _e('Password','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="confirmterms" id="confirmterms" required>
								<?php echo sprintf(__('I agree to the <a class="link-muted" href="%s/terms">Terms and Conditions</a>','skeda'),get_bloginfo("url"));?>
							</label>
							<div class="help-block with-errors"></div>
						</div>
					</div>


					<p class="text-right my-3">
						<button type="submit" class="btn btn-block btn-primary btn-block"><?php _e('Sign up','skeda');?></button>
					</p>

					<?php wp_nonce_field('signup', '_wpnonce');?>

					<span class="text-muted"><?php _e('Already have an account?','skeda');?></span>
					<a class="" href="<?php bloginfo('url');?>/login"><?php _e('Login','skeda');?></a>
				</form>

				<?php }?>
			</div>
		</div>
	</div>
</main>
<?php get_footer('');?>