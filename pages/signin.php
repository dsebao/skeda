<?php

/*
Template Name: Sign in
*/

if(is_user_logged_in()){
	wp_redirect(home_url('/'));
}

$state = getparams('action');

get_header();

?>
<main class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-4 mx-md-auto my-md-5">
				<?php if($state == 'recover'){?>
				<form id="recover" class="s_form mx-md-auto mb-5" data-toggle="validator" action="" method="post">
					<div class="mb-7">
						<h3 class="h3 text-primary font-weight-normal mb-0"><?php _e('Recover your password','skeda');?> <span class="font-weight-semi-bold"><?php _e('back','skeda');?></span></h3>
						<div class="message"></div>
					</div>

					<div class="form-group has-feedback">
						<input type="email" class="form-control" name="email" id="email" placeholder="<?php _e('Your email','skeda');?>" aria-label="<?php _e('Your email','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>

					<?php wp_nonce_field('signin', '_wpnonce');?>

					<button type="submit" class="btn btn-primary btn-block mb-3"><?php _e('Sign in','skeda');?></button>
				</form>

				<?php } elseif($state == 'resetpass' && isset($userreset)){?>

				<form id="resetpass" class="s_form mx-md-auto mb-5" data-toggle="validator" action="" method="post">
					<div class="mb-7">
						<h3 class="h3 text-primary font-weight-normal mb-0"><?php _e('Set your new password','skeda');?></h3>
						<hr>
						<div class="message"></div>
					</div>

					<div class="form-group">
						<input type="password" class="form-control" name="pass" id="pass" placeholder="********" aria-label="********" required="">
						<div class="help-block with-errors"></div>
					</div>

					<input type="hidden" name="useremail" value="<?php echo $_GET['useremail'];?>">

					<?php wp_nonce_field('signin', '_wpnonce');?>

					<button type="submit" class="btn btn-primary btn-block mb-3"><?php _e('Save','skeda');?></button>
				</form>
				<?php } elseif($state == 'resendvalidation'){?>
				<form id="resendvalidation" class="s_form mx-md-auto mb-5" data-toggle="validator" action="" method="post">
					<div class="mb-7">
						<h3 class="h3 text-primary font-weight-normal mb-0"><?php _e('Resend email validation','skeda');?></h3>
						<hr>
						<div class="message"></div>
					</div>

					<div class="form-group has-feedback">
						<label class="form-label" for="email"><?php _e('Email Address','skeda');?></label>
						<input type="email" class="form-control" name="email" id="email" placeholder="<?php _e('Email Address','skeda');?>" aria-label="Email address" required="">
						<div class="help-block with-errors"></div>
					</div>

					<?php wp_nonce_field('signin', '_wpnonce');?>

					<button type="submit" class="btn btn-primary btn-block mb-3"><?php _e('Resend code','skeda');?></button>
				</form>
				<?php } else {?>
				<form id="login" class="mx-md-auto mb-5" data-toggle="validator" action="" method="post">
					<div class="mb-7">
						<h1 class="h2 font-weight-normal mb-3"><?php _e('Sign in','skeda');?></h1>
					</div>

					<div class="message">
						<?php echo (isset($message)) ? $message : "";?>
					</div>

					<div class="form-group has-feedback">
						<input type="email" class="form-control" name="email" id="email" placeholder="<?php _e('Email Address','skeda');?>" aria-label="<?php _e('Email Address','skeda');?>" required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="pass" id="pass" placeholder="********" aria-label="********" required="">
						<div class="help-block with-errors"></div>
					</div>

					<div class="text-right mb-3">
						<?php echo sprintf(__("<a href='%s' class='text-muted'>Forgot Password?</a>",'skeda'),get_bloginfo('url') . "/login?action=recover");?>
					</div>

					<?php wp_nonce_field('signin', '_wpnonce');?>

					<button type="submit" class="btn btn-primary btn-block mb-3"><?php _e('Login','skeda');?></button>

					<p class="text-center">
						<span class="text-muted"><?php _e('Don\'t have an account?','skeda');?></span>
						<a class="" href="<?php bloginfo('url');?>/signup"><?php _e('Signup','skeda');?></a>
					</p>
				</form>
				<?php }?>
			</div>
		</div>
	</div>
</main>
<?php get_footer('login');?>