<?php
/*
*
*
* Custom avatar
*
*
*/


function get_avatarimg_url($get_avatar){
    preg_match('#src=["|\'](.+)["|\']#Uuis', $get_avatar, $matches);
    return $matches[1];
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



function getOptionsCustom($options,$selected = ''){
      foreach ( $options as $a ) {
        $z = ($a == $selected) ? 'selected' : "";
        echo "<option value='" . $a . "' ".$z.">" .$a ."</option>";
    }
}

function custom_query_vars_filter($vars) {
  $vars[] .= '_ekey';
  $vars[] .= 'user';
  $vars[] .= 'action';
  $vars[] .= 'useremail';
  return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter' );

add_filter('get_avatar_url', 'set_https', 10, 3);
function set_https($url, $id_or_email, $args){
    return set_url_scheme( $url, 'https' );;
}

function custom_avatars($avatar, $id_or_email, $size){
    $image_url = get_user_meta($id_or_email, 'user_avatar', true);
    if($image_url !== '' && is_int($id_or_email))
        $return = '<img src="'.$image_url.'" class="avatar-img" width="'.$size.'" height="'.$size.'"/>';
    elseif($avatar)
        $return = $avatar;
    return $return;
}
add_filter('get_avatar', 'custom_avatars', 10, 5);

function getparams($g){
    if(isset($_GET[$g])){
        return $_GET[$g];
    } else {
        return '';
    }
}


/**
 * Add a general nonce to requests
 */
function add_general_nonce(){
    $nonce = wp_create_nonce( 'noncefield' );
    echo "<meta name='csrf-token' content='$nonce'>";
}
// To add to front-end pages
add_action( 'wp_head', 'add_general_nonce' );


/**
 * Verify the submitted nonce
 */
function verify_general_nonce(){
    $nonce = isset($_SERVER['HTTP_X_CSRF_TOKEN']) ? $_SERVER['HTTP_X_CSRF_TOKEN']: '';
    if (!wp_verify_nonce( $nonce, 'noncefield')) {
        die();
    }
}



function checklogin($where = ''){
	if (!is_user_logged_in()) {
  		wp_redirect(home_url() . $where);
	}
}

function get_the_blog_name() {
    $blog_id = get_current_blog_id();
    $blog_info = get_blog_details($blog_id);
    $blogname = $blog_info->blogname;
    return $blogname;
}

function maybeAddUserRoles($blog_id){
    if(get_option('wp_user_roles')){
      update_option('wp_'.$blog_id.'_user_roles', get_option('wp_user_roles'));
      delete_option('wp_user_roles');
    }
    //Change template
    switch_theme('skeda-business');

}

function createPagesNewSite(){
    $page = array(
        'post_title'    => 'Home',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'front/home.php' );
    update_option('page_on_front', $theid);
    update_option('show_on_front', 'page');

    $page = array(
        'post_title'    => 'Dashboard',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
    );
    $dashboard = wp_insert_post($page);
    update_post_meta( $dashboard, '_wp_page_template', 'dashboard/dashboard.php' );

    $page = array(
        'post_title'    => 'My account',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_parent' => $dashboard,
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'dashboard/my-account.php' );

    $page = array(
        'post_title'    => 'Upcoming',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_parent' => $dashboard,
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'dashboard/upcoming.php' );

    $page = array(
        'post_title'    => 'Previous',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_parent' => $dashboard,
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'dashboard/previous.php' );

    $page = array(
        'post_title'    => 'Complaints',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_parent' => $dashboard,
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'dashboard/complaints.php' );

    $page = array(
        'post_title'    => 'Customer',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_parent' => $dashboard,
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'dashboard/customer.php' );

    $page = array(
        'post_title'    => 'Bills',
        'post_type' => 'page',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_parent' => $dashboard,
    );
    $theid = wp_insert_post($page);
    update_post_meta( $theid, '_wp_page_template', 'dashboard/bills.php' );

}

function addSuperAdmintoSite($id){
    $a = get_super_admins();
    foreach ($a as $k) {
        $a = get_user_by('login', $k);
        add_user_to_blog($id,$a->ID,'administrator');

        $capuser = new WP_User($a->ID);
        $capuser->add_role('wpamelia-manager',__('Amelia Manager', 'skeda'),
            array('read' => true,'amelia_read_m'=> true,'amelia_read_dashboard'=> true,'amelia_read_calendar'=> true,'amelia_read_appointments'  => true,'amelia_read_events'=> true,'amelia_read_employees' => true,'amelia_read_services'  => true,'amelia_read_locations' => true,'amelia_read_coupons'   => true,'amelia_read_customers' => true,'amelia_read_finance'   => true,'amelia_read_notifications' => true,'amelia_read_others_calendar'   => true,'amelia_read_others_appointments'  => true,'amelia_read_others_employees'  => true,'amelia_write_dashboard'    => true,'amelia_write_calendar' => true,'amelia_write_appointments' => true,'amelia_write_events'   => true,'amelia_write_employees'    => true,'amelia_write_services' => true,'amelia_write_locations'    => true,'amelia_write_coupons' => true,'amelia_write_customers' => true,'amelia_write_finance' => true,'amelia_write_notifications' => true,'amelia_write_others_calendar' => true,'amelia_write_others_appointments' => true,'amelia_write_others_employees' => true,'amelia_write_others_events' => true,'amelia_write_others_finance' => true,'amelia_write_others_dashboard' => true,'amelia_write_status_appointments' => true,'amelia_write_time_appointments' => true,'upload_files' => true)
        );
    }
}

function redirectToBlog($userid){
    $user_info = get_userdata($userid);
    if ($user_info->primary_blog){
        $primary_url = get_blogaddress_by_id($user_info->primary_blog) /* . 'wp-admin/'*/;
        $user_blogs = get_blogs_of_user($user->ID);

        //Loop and see if user has access
        $allowed = false;
        foreach($user_blogs as $user_blog){
            if($user_blog->userblog_id == $blog_id){
                $allowed = true;
                break;
            }
        }

        //Let users login to others blog IF we can get their primary blog URL and they are not allowed on this blog
        if ($primary_url && !$allowed){
            wp_redirect($primary_url . "/wp-admin/");
            die();
        }
    }
}



function skeda_block_wp_admin() {
    if ( is_admin() && !is_super_admin() && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_safe_redirect(home_url());
        exit;
    }
}
add_action( 'admin_init', 'skeda_block_wp_admin' );

function login_bottom_extra(){
    echo "<div id='nav' style='margin-bottom:40px'><a href='".get_bloginfo('url')."/signup'>" . __('Don\'t have an account? Sign Up','skeda') . "</a></div>";
}

add_action('login_footer','login_bottom_extra');

/**
 * Add any custom tables that this plugin creates for an individual site to the list
 *  of tables that get deleted when a site is deleted.
 * @see https://core.trac.wordpress.org/browser/tags/4.2.2/src/wp-admin/includes/ms.php#L116
 */
add_filter( 'wpmu_drop_tables', 'delete_my_plugin_tables', 10, 2 );
function delete_my_plugin_tables( $tables=array(), $blog_id=null ) {
    /**
     * Make sure the blog ID parameter was sent, so we don't
     *  accidentally delete tables for the wrong blog
     */
    if ( empty( $blog_id ) || 1 == $blog_id || $blog_id != $GLOBALS['blog_id'] )
        return $tables;

    /**
     * Assume our plugin added three new tables called "plugin_table_1", "plugin_table_2" and "plugin_table_3"
     *  to each site on which it's active
     */
    global $wpdb;
    $blog_prefix = $wpdb->get_blog_prefix( $blog_id );
    $base_prefix = $wpdb->base_prefix;
    $plugin_tables = array( 'amelia_users', 'amelia_services', 'amelia_payments' );
    /**
     * Since the $wpdb->tables() call in the wpmu_delete_blog() function is called without the
     *  $prefix parameter, the list of tables sent through this filter will all be prefixed
     *  with the appropriate blog prefix before it gets to this filter. We need to prefix
     *  our list of tables, as well, before sending it back.
     */
    foreach ( $plugin_tables as $k => $table ) {
        $tables[$table] = $blog_prefix . $table;
    }

    return $tables;
}

remove_filter('authenticate', 'wp_authenticate_username_password');

add_filter('gettext', function($text){
    if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
        if('Username or Email Address' == $text || 'Имя пользователя или e-mail' == $text){
            return __('Email','skeda');
        }
    }
    return $text;
}, 20);

