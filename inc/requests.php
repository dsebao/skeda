<?php

if(isset($_GET['action']) && $_GET['action'] == 'resetpass' && isset($_GET['key']) && isset($_GET['useremail'])){
    $u = get_user_by('email',$_GET['useremail']);
    if($u){
        global $wpdb;
        $user = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->users WHERE user_activation_key = %s AND user_email = %s", $_GET['key'], $_GET['useremail']));
        if (!empty($user)){
            $userreset = $user;
        }
    }
}


//Signup
if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'signup' )){

    $errores = array();

    //Check validation of variables
    (isset($_POST['personname']) && $_POST['personname'] != "") ? $name = $_POST['personname'] : $errores[] = __('Please fill your name','skeda');
    (isset($_POST['personlastname']) && $_POST['personlastname'] != "") ? $lastname = $_POST['personlastname'] : $errores[] = __('Please fill your surname','skeda');
    (isset($_POST['email']) && $_POST['email'] != "") ? $email = $_POST['email'] : $errores[] = __('Please fill your email','skeda');
    (isset($_POST['phone']) && $_POST['phone'] != "") ? $phone = $_POST['phone'] : $errores[] = __('Please fill your phone','skeda');
    (isset($_POST['pass']) && $_POST['pass'] != "") ? $pass = $_POST['pass'] : $errores[] = __('Please define a password','skeda');

    if(!is_email($email)){
        $errores[] = __('Email invalid','skeda');
    }


    if(email_exists($email))
        $errores[] = __('Email already registered','skeda');

    //if all is cimplete process the user creation and blog creation
    if(empty($errores)){

        $user_id = wp_create_user($email, $pass, $email);

        if (intval($user_id)){

            //Create user and assign them to root blog

            $user = new WP_User($user_id);
            $user->set_role('author');

            wp_update_user( array ('ID' => $user->ID,'display_name' => sanitize_text_field($name ." " .$lastname),'first_name' => $name,'last_name' => $lastname,'user_nicename' => sanitize_text_field($name . " " . $lastname)));

            wp_set_password($pass,$user->ID);
            wp_clear_auth_cookie();

            //generate key validation
            $ekey = md5(uniqid());
            update_user_meta($user->ID,'_ekey',$ekey);

            //Save data business
            $databusiness = array('phone' => $phone,'sitecreated' => 0,'name' => $name,'lastname' => $lastname);
            update_user_meta($user->ID, 'business_data', $databusiness);

            $link = get_bloginfo('url') ."/signup/?_ekey=" . $ekey . "&user=" . $user->ID;
            $button = htmlButton($link,__('Confirm email','skeda'));
            $content = "<h2>" . __( 'Hi!', 'skeda' ) . " " . $name ."!</h2><p>" . __( 'Welcome to our platform. To validate your user and to start using Skēda please click on the link below.', 'skeda' ) . "</p><p style='margin-top:25px;margin-bottom:25px'>$button<br>";
            $subject = __( 'Please confirm your account', 'skeda');

            //Send the email to the new user
            $sent = sendNotification($subject,$content,$email);

            //wp_mail($GLOBALS['ADMINEMAIL'], __('New user registration','skeda'), $subject. ": " . $name . ' (' . $email . ')');

            wp_redirect(home_url() . "/signup/?action=validatemail");
            exit();
        } else {

            $error_string = $user_id->get_error_message();

            $message = "<div class='alert alert-warning'>$error_string</div>";
        }
    } else {
        $error_message = '';
        foreach ($errores as $v) {
            $error_message .= "<span>".$v."</span>";
        }
        $message = "<div class='alert alert-warning'>$error_message</div>";
    }
}

//User created, now create the site
if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'createsite')){
    $errores = array();

    if(isset($_COOKIE['site_userid']) && $_COOKIE['site_userid'] == $_POST['userid']){
        $user = $_POST['userid'];
    } else{
        $errores[] = __('Something went wrong','skeda');
    }

    $u = get_user_by('id',$user);

    //Check validation of variables
    (isset($_POST['businesstype']) && $_POST['businesstype'] != "") ? $type = $_POST['businesstype'] : $errores[] = __('Please select the type of business','skeda');
    (isset($_POST['business']) && $_POST['business'] != "") ? $business = $_POST['business'] : $errores[] = __('Please define a business name','skeda');
    (isset($_POST['position']) && $_POST['position'] != "") ? $position = $_POST['position'] : $errores[] = __('Please define your position','skeda');
    (isset($_POST['url']) && $_POST['url'] != "") ? $url = $_POST['url'] : $errores[] = __('Please define your site url','skeda');


    //if all is cimplete process the user creation and blog creation
    if(empty($errores) && is_object($u)){

        //Create new blog
        $a = array(
            'domain' => sanitize_title($url) . '.' .$_SERVER['HTTP_HOST'],
            'path' => '/',
            'user_id' => $user,
            'title' => $business
        );
        $newblog = wp_insert_site($a);

        if (!is_wp_error($newblog)){

            /*
            Actions for the new site
            */
            switch_to_blog($newblog);

                maybeAddUserRoles($newblog);

                addSuperAdmintoSite($newblog);

                switch_theme('skeda-business');

                createPagesNewSite();

                $databusiness = get_user_meta(intval($_GET['user']),'business_data',true);
                //Save data business
                $databusiness['business'] = $business;
                $databusiness['typebusiness'] = $type;
                $databusiness['position'] = $position;
                $databusiness['url'] = $url;
                $databusiness['sitecreated'] = 1;
                $databusiness['sitecreated'] = 1;
            

                update_user_meta(intval($user), '_ekey','');
                update_user_meta(intval($user), 'business_data', $databusiness);

                update_option('skedacompany',array('skeda_data_name' => $business,'skeda_data_currency' => 'RUB','skeda_data_time' => 'UTC+3','skeda_data_date' => 'd-m-Y','skeda_data_hour' => 'H:i','skeda_data_lang' => 'ru_RU'));
                update_option('thumbnail_size_w','170');
                update_option('thumbnail_size_h','170');
                update_option('notifications_translated','no');
                update_option('date_format','d-m-Y');
                update_option('time_format','H:i');

                $capuser = new WP_User($user);
                $capuser->set_role('administrator');
                $capuser->add_role('wpamelia-manager',__('Amelia Manager', 'skeda'),
                    array('read' => true,'amelia_read_m'=> true,'amelia_read_dashboard'=> true,'amelia_read_calendar'=> true,'amelia_read_appointments'  => true,'amelia_read_events'=> true,'amelia_read_employees' => true,'amelia_read_services'  => true,'amelia_read_locations' => true,'amelia_read_coupons'   => true,'amelia_read_customers' => true,'amelia_read_finance'   => true,'amelia_read_notifications' => true,'amelia_read_others_calendar'   => true,'amelia_read_others_appointments'  => true,'amelia_read_others_employees'  => true,'amelia_write_dashboard'    => true,'amelia_write_calendar' => true,'amelia_write_appointments' => true,'amelia_write_events'   => true,'amelia_write_employees'    => true,'amelia_write_services' => true,'amelia_write_locations'    => true,'amelia_write_coupons' => true,'amelia_write_customers' => true,'amelia_write_finance' => true,'amelia_write_notifications' => true,'amelia_write_others_calendar' => true,'amelia_write_others_appointments' => true,'amelia_write_others_employees' => true,'amelia_write_others_events' => true,'amelia_write_others_finance' => true,'amelia_write_others_dashboard' => true,'amelia_write_status_appointments' => true,'amelia_write_time_appointments' => true,'upload_files' => true)
                );

            restore_current_blog();


            $link = get_admin_url($newblog);
            $button = htmlButton($link,__('Sign in','skeda'));
            $content = "<h2>" . __( 'Your site is ready', 'skeda' ) . " " . $u->display_name ."!</h2><p>" . __( 'Your site is ready to use. Simply login with your user information in the link below.', 'skeda' ) . "</p><p style='margin-top:25px;margin-bottom:25px'>$button<br>";
            $subject = __( 'Your site is ready', 'skeda');

            //Send the email to the new user
            $sent = sendNotification($subject,$content,$u->user_email,true);

            //wp_mail($GLOBALS['ADMINEMAIL'], __('New user registration','skeda'), $subject. ": " . $name . ' (' . $email . ')');

            //Remove user from main blog
            remove_user_from_blog($user,1);

            wp_redirect(home_url() . "/signup/?action=sitecreated&id=" . $newblog);
            exit();
        } else {
            $error_string = $newblog->get_error_message();
            $message = "<div class='alert alert-warning'>$error_string</div>";
        }
    } else {
        $error_message = '';
        foreach ($errores as $v) {
            $error_message .= "<span>".$v."</span>";
        }
        $message = "<div class='alert alert-warning'>$error_message</div>";
        $confirmedemail = true;
    }
}

// User email validation

if(isset($_GET['_ekey']) && $_GET['_ekey'] != '' && isset($_GET['user']) && $_GET['user'] != ''){
    $d = get_user_meta(intval($_GET['user']),'_ekey',true);
    $data = get_user_meta(intval($_GET['user']),'business_data',true);
    if($d === $_GET['_ekey'] && $data['sitecreated'] == 0){
        //Confirmed that clicked in the email and not creating a site, get to the form now
        $confirmedemail = true;
        //Saved the user id in a cookie
        setcookie("site_userid", $_GET['user'], time()+3600 * 24);
    }
}

//SignIn
if (isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'signin' )){

    $errores = array();

    wp_clear_auth_cookie();

    $creds = array();
    $creds['user_email'] = sanitize_email($_POST['email']);
    $creds['user_password'] = esc_attr($_POST['pass']);
    $creds['remember'] = true;

    //Detect if validated the email
    //User exist?
    $u = get_user_by('email',$creds['user_email']);
    $check = (is_object($u)) ? wp_authenticate_username_password( NULL, $u->user_login,$creds['user_password']) : new WP_Error('error');

    if(is_object($u))
        $creds['user_login'] = $u->user_login;

    if(!is_wp_error($check)){
        //If user exist, detect if key exist
        $validatekey = get_user_meta($check->ID, '_ekey',true);
        //If the key is empty signon the user if not thrown an error
        $validated = ($validatekey == '') ? true : false;

        if($validated){
            //If the user has validate is email
            $user = wp_signon($creds, false);
            wp_set_auth_cookie($user->ID,true);
            wp_set_current_user($user->ID);

            redirectToBlog($user->ID);

            exit();

            //wp_redirect(get_bloginfo('url') . "/wp-admin/");

        } else {
            $urllink = get_bloginfo('url') . "/signin?action=resendvalidation";
            $message = "<div class='alert alert-warning'>". __( 'You must validate your email.', 'skeda') . "<br><a href='" . $urllink . "'>" . __( 'Resend validation', 'skeda') . "</a></div>";
        }

    } else {
        //Error on the credentials
        $message = "<div class='alert alert-warning'>".__('User invalid','skeda')."</div>";
    }
}




?>