<?php


function sendPublicform(){
	//SIGNUP
	if($_POST['typeform'] == 'form_signup'){
		verify_general_nonce();

		$email = sanitize_email($_POST['email']);
		$username = sanitize_user($_POST['username']);
		$pass = $_POST['pass'];
		$hash = wp_hash_password($pass);
		if(is_email($email))
			new WP_Error( 'error', __('Invalid email','growlink'));
		$user_id = wp_create_user($username, $hash, $email);

		if (!is_wp_error( $user_id )) {
			$user = new WP_User($user_id);
			$user->set_role('author');
			wp_update_user( array ('ID' => $user->ID,'display_name' => $username,'user_nicename' => sanitize_text_field($username)));
			wp_set_password($pass,$user->ID);
			wp_clear_auth_cookie();

			//Create referral if was reffered by an affiliate
			$referral_id   = 0;
			$referral_args = array();


			//generate key for validation
			$link = md5(uniqid());
			update_user_meta($user->ID,'_data_user_key',$link);

			//Send notification email
			$subject = __( 'Please confirm your account', 'growlink' );
			$link = get_bloginfo('url') ."/login?_emailvalidation=" . $link . "&user=" . $user->ID;
			$button = htmlButton($link,__('Confirm email','growlink'));
			$content = "<h2>" . __( 'Hi!', 'growlink' ) . " " . $username ."!</h2><p>" . __( 'Welcome to our platform. To validate your account click on the following link', 'growlink' ) . "</p><p style='margin-top:25px;margin-bottom:25px'>$button<br>";

			$sent = sendNotification($subject,$content,$email,true);
			wp_mail($GLOBALS['ADMINEMAIL'], __('New user registration','growlink'), $subject. ": " . $username . ' (' . $email . ')');

			$createbio = array(
				'post_title'    => $username,
				'post_content'  => '',
				'post_status'   => 'publish',
				'post_author'   => $user->ID,
				'post_type' => 'post'
   			);
   			$idpost = wp_insert_post($createbio);

   			$datapost = array('site-title' => $username,'site-intro' => '','site-hero' => 'empty','site-herodata' =>'','site-font' => 'Barlow','site-color-text' => '#333333','site-border' => '16','site-bgcolor' => '#f8f8f8','site-color-link' => '#3C28AD','site-roundedcorners' => 'rounded','site-viewdesign' => 'list');
			update_post_meta($idpost, 'design_data', $datapost);

			$optiondata = array('option-fbpixel' => '','option-ga' => '','option-mailchimp' => '','option-mailchimp-list' => '','option-titletag' => '','option-dsc' => '','option-ographimg' => '','option-favicon' => '');
			update_post_meta($idpost, 'option_data', $optiondata);

			//Echo the result in json
			if($idpost){
		    	wp_send_json(array('action'=> 'signup','type' => 'success','message'=>  __( 'We just sent you an email to verify your user. Please check your inbox.', 'growlink' ),'url' => '','resetform' => true));
		    }
		} else {
			$error_string = $user_id->get_error_message();
			wp_send_json(array('type'=> 'danger','action' => 'error','message'=>  $error_string,'error' => $error_string,'resetform' => false));
		}
	}

	//LOGIN
	if($_POST['typeform'] == 'form_login'){
		wp_clear_auth_cookie();
		verify_general_nonce();

		$creds = array();
		$creds['user_email'] = sanitize_email($_POST['email']);
		$creds['user_password'] = esc_attr($_POST['pass']);
		$creds['remember'] = true;

		//Detect if validated the email
		//User exist?
		$u = get_user_by('email',$creds['user_email']);
		$check = (is_object($u)) ? wp_authenticate_username_password( NULL, $u->user_login,$creds['user_password']) : new WP_Error('error');
		$creds['user_login'] = $u->user_login;

		if(!is_wp_error($check)){
			//If user exist, detect if key exist
			$validatekey = get_user_meta($check->ID, '_data_user_key',true);
			//If the key is empty signon the user if not thrown an error
			$validated = ($validatekey == '') ? true : false;

			if($validated){
				//If the user has validate is email
				$user = wp_signon($creds, false);
				wp_set_auth_cookie($user->ID,true);
				wp_set_current_user($user->ID);
				$tourl = add_query_arg('language', 'es', home_url('dashboard'));
				wp_send_json(array('success'=> true,'url' => $tourl));
			} else {
				$urllink = get_bloginfo('url') . "/login?action=resendvalidation";
				$messagebox = __( 'You must validate your email.', 'growlink') . "<br><a href='" . $urllink . "'>" . __( 'Resend validation', 'growlink') . "</a>";
				wp_send_json(array('action' => 'novalidate','type' => 'warning','message'=> $messagebox,'resetform' => true));
			}

		} else {
			//Error on the credentials
			wp_send_json(array('action' => 'nouser','type' => 'warning', 'message'=> __( 'Wrong credentials', 'growlink'),'resetform' => true));
		}
	}

	if($_POST['typeform'] == 'form_recover'){

		$email = sanitize_email($_POST['email']);

		$u = get_user_by('email',$email);

		if ($u){
			$user_login = $u->user_login;
        	$user_email = $u->user_email;
        	$key = substr( md5( uniqid( microtime() ) ), 0, 8);
        	global $wpdb;
        	$wpdb->query("UPDATE $wpdb->users SET user_activation_key = '$key' WHERE user_login = '$user_login'");

        	$subject = __( 'Reset your password', 'growlink' );
			$link = get_bloginfo('url') ."/login?action=resetpass&key=" . $key . "&useremail=" . $email;
			$button = htmlButton($link,__('Reset password','growlink'));
			$content = "<p>" . __( 'Someone has asked to reset the password of your profile in Growlink. To reset your password visit the following address:', 'growlink' ) . "</p><p style='margin-top:25px;margin-bottom:25px'>".$button."<br>";

			$sent = sendNotification($subject,$content,$email,true);

			wp_send_json(array('action' => 'resetok','type' => 'success','message'=> __('The instructions have been sent to your email','growlink'),'resetform' => true));

		} else {
			wp_send_json(array('action' => 'nouser','type' => 'danger','message'=> __('Username does not exist','growlink'),'resetform' => true));
		}
	}

	if($_POST['typeform'] == 'form_resetpass'){
		$u = get_user_by('email',$_POST['useremail']);
		$pass = $_POST['pass'];
	    if($u){
	    	wp_set_password($pass,$u->ID);
	    	wp_send_json(array('success' => true,'action' => 'resetok','type' => 'success','message'=> __(''),'resetform' => true,'url' => get_bloginfo('url') . "/login?action=resetpasssuccess"));
	    } else {
	    	wp_send_json(array('action' => 'nouser','type' => 'danger','message'=> __('Username does not exist','growlink'),'resetform' => true));
	    }
	}

	if($_POST['typeform'] == 'form_resendvalidation'){
		$u = get_user_by('email',$_POST['email']);
	    if($u){
	    	$validatekey = get_user_meta($u->ID, '_data_user_key',true);
	    	if($validatekey != ''){
	    		$link = md5(uniqid());
				update_user_meta($u->ID,'_data_user_key',$link);

				//Send notification email
				$subject = __( 'Validate your email', 'growlink' );
				$link = get_bloginfo('url') ."/login?_emailvalidation=" . $link . "&user=" . $u->ID;
				$content = "<p>" . __( 'You have resend the link to validate your acount. Please click the next link.', 'growlink' ) . "</p><p style='margin-top:25px;margin-bottom:25px'><a href='".$link."' style='background: #4137CF;color:#ffffff;padding:14px 30px;text-decoration:none;font-size:17px;text-align-center;border-radius:6px' target='_blank' title=''>Confirm email</a><br>";

				$sent = sendNotification($subject,$content,$u->user_email,true);
	    	}
	    	wp_send_json(array('action' => 'resendcode','type' => 'success','message'=> __('We have just send the link to yor inbox.','growlink'),'resetform' => true));
	    } else {

	    }
	}

	if($_POST['typeform'] == 'form_contact'){
		$email = sanitize_email($_POST['email']);
		$name = sanitize_text_field($_POST['name']);
		$message = sanitize_text_field($_POST['message']);

		if(is_email($email)){
			$subject = __( 'Contact from Skeda', 'skeda' );
			$content = "<p>".__('Name','skeda').": $name<br>".__('Email','skeda').": $email<br>".__('Message','skeda').":<br>$message</p>";
			$sent = sendNotification($subject,$content,$GLOBALS['ADMINEMAIL'],true);
			if($sent)
				wp_send_json(array('action' => 'contact','type' => 'success','message'=> __('Thanks for writing to us. We will respond you as soon as possible.','skeda'),'resetform' => true));

		} else {
			wp_send_json(array('action' => 'nouser','type' => 'danger','message'=> __('An error ocurred.','growlink'),'resetform' => true));
		}
	}
}

add_action('wp_ajax_sendPublicform', 'sendPublicform');
add_action('wp_ajax_nopriv_sendPublicform', 'sendPublicform');


?>