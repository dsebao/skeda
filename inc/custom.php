<?php

function skeda_getoption( $key = '', $default = false ) {
    if ( function_exists( 'cmb2_get_option' ) ) {
        // Use cmb2_get_option as it passes through some key filters.
        return cmb2_get_option( 'skeda_option', $key, $default );
    }
    // Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option( 'skeda_option', $default );
    $val = $default;
    if ( 'all' == $key ) {
        $val = $opts;
    } elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
        $val = $opts[ $key ];
    }
    return $val;
}

add_action( 'cmb2_admin_init', 'skeda_optiondata' );
function skeda_optiondata() {
    $cmb_options = new_cmb2_box( array(
        'id'           => 'skeda_option',
        'title'        => 'Options',
        'object_types' => array( 'options-page' ),
        'option_key'      => 'skeda_option', // The option key and admin menu page slug.
        'icon_url'        => 'dashicons-networking', // Menu icon. Only applicable if 'parent_slug' is left empty.
        'capability'      => 'edit_posts', // Cap required to view options-page.
    ));

    $cmb_options->add_field( array(
        'name' => 'Integrations API',
        'type' => 'title',
        'id'   => 'title'
    ) );

    $cmb_options->add_field( array(
        'name' => 'Scripts',
        'type' => 'title',
        'id'   => 'title2'
    ));

    $cmb_options->add_field(
        array(
            'name'       => 'Header scripts',
            'id'         => 'header_scripts',
            'type' => 'textarea_code',
        )
    );

     $cmb_options->add_field(
        array(
            'name'       => 'Footer scripts',
            'id'         => 'footer_scripts',
            'type' => 'textarea_code',
        )
    );
}

add_action( 'cmb2_admin_init', 'skeda_metapages' );
function skeda_metapages() {
    $prefix = 'growlink_';
    $cmb_demo = new_cmb2_box( array(
        'id'            => $prefix . 'pagemeta',
        'title'         => esc_html__( 'SEO', 'growlink' ),
        'object_types'  => array('page'), // Post type
    ));

    $cmb_demo->add_field( array(
        'name'       => esc_html__( 'Meta title', 'growlink' ),
        'id'         => $prefix . 'metatitle',
        'type'       => 'text',
    ) );

    $cmb_demo->add_field( array(
        'name'       => esc_html__( 'Meta Description', 'growlink' ),
        'id'         => $prefix . 'metadesc',
        'type'       => 'text',
    ) );

    $cmb_demo->add_field( array(
        'name'       => esc_html__( 'Meta Keywords', 'growlink' ),
        'id'         => $prefix . 'metakeys',
        'type'       => 'text',
    ) );
}

// Maybe change the wp_die_handler.
add_filter( 'wp_die_handler', function( $handler ) {
    return 'themed_wp_die_handler';
}, 10);

// Use a custom wp_die() handler.
function themed_wp_die_handler( $message, $title = '', $args = array() ) {
    $defaults = array( 'response' => 500 );
    $r = wp_parse_args($args, $defaults);
    if ( function_exists( 'is_wp_error' ) && is_wp_error( $message ) ) {
        $errors = $message->get_error_messages();
        switch ( count( $errors ) ) {
            case 0 :
                $message = '';
                break;
            case 1 :
                $message = $errors[0];
                break;
            default :
                $message = "<ul>\n\t\t<li>" . join( "</li>\n\t\t<li>", $errors ) . "</li>\n\t</ul>";
                break;
        }
    } else {
        $message = $message;
    }
    require_once get_stylesheet_directory() . '/wp-die.php';
    die();
}

function updateAmeliaUser($blogid,$data){
    global $wpdb;
    $query = $wpdb->insert($wpdb->base_prefix.$blogid."_amelia_users",$data);
    return $query;
}



