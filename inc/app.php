<?php
/*
Site config
*/

//Variables globales
$GLOBALS['SITENAME'] = 'SKEDA TEAM';
$GLOBALS['DOMAIN'] = 'skeda.ru';
$GLOBALS['ADMINEMAIL'] = get_option('admin_email');
$GLOBALS['BUSINESS_TYPE'] = array(__('Beauty salon','skeda'),__('Beauty salon chain ','skeda'),__('Individual Professional','skeda'),__('Small beauty studio','skeda'));
$GLOBALS['BUSINESS_POSITION'] = array(__('Salon’s owner','skeda'),__('Salon’s director','skeda'),__('Salon’s administrator ','skeda'),__('Private professional','skeda'));
$GLOBALS['emailtemplate'] = '<html><head><meta name="viewport" content="width=device-width"> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <title>{{subject}}</title> <style media="all" type="text/css">body{color: #3d4868;}@media all{.btn-primary table td:hover{background-color: #34495e !important;}.btn-primary a:hover{background-color: #34495e !important; border-color: #34495e !important;}}@media all{.btn-secondary a:hover{border-color: #34495e !important; color: #34495e !important;}}@media only screen and (max-width: 620px){table[class=body] h1{font-size: 28px !important; margin-bottom: 10px !important;}table[class=body] h2{font-size: 22px !important; margin-bottom: 10px !important;}table[class=body] h3{font-size: 16px !important; margin-bottom: 10px !important;}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size: 16px !important;}table[class=body] .wrapper, table[class=body] .article{padding: 10px !important;}table[class=body] .content{padding: 0 !important;}table[class=body] .container{padding: 0 !important; width: 100% !important;}table[class=body] .header{margin-bottom: 10px !important;}table[class=body] .main{border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important;}table[class=body] .btn table{width: 100% !important;}table[class=body] .btn a{width: 100% !important;}table[class=body] .img-responsive{height: auto !important; max-width: 100% !important; width: auto !important;}table[class=body] .alert td{border-radius: 0 !important; padding: 10px !important;}table[class=body] .span-2, table[class=body] .span-3{max-width: none !important; width: 100% !important;}table[class=body] .receipt{width: 100% !important;}}@media all{.ExternalClass{width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}.apple-link a{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important;}}</style> </head> <body class="" style="font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f6f6f6; margin: 0; padding: 0;"> <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;" width="100%" bgcolor="#f6f6f6"> <tbody><tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td><td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto !important; max-width: 580px; padding: 10px; width: 580px;" width="580" valign="top"> <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;"> <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;"></span> <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;" width="100%"> <tbody><tr> <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;" valign="top"> <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%"> <tbody><tr> <td style="font-family: sans-serif; font-size: 30px; vertical-align: top;" valign="top"> <span style="/* letter-spacing:4px; */font-weight:700;color: #44444;">Skēda</span> </td></tr><tr> <td height="30"><hr style="border-color: #ececec;border-width: 1px;"></td></tr><tr> <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">{{content}}<br></td></tr></tbody></table> </td></tr></tbody></table> <div class="footer" style="clear: both; padding-top: 10px; text-align: center; width: 100%;"> <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%"> </table> </div></div></td><td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td></tr></tbody></table> </body></html>';


add_action( 'init', 'stop_heartbeat', 1 );
function stop_heartbeat() {
    wp_deregister_script('heartbeat');
}

if (defined('WP_DEBUG_LOG') && WP_DEBUG_LOG) {
    error_reporting(E_ALL); ini_set('display_errors', 1);
    ini_set( 'error_log', get_template_directory_uri() . '/log.txt' );
}

function load_custom_wp_admin_style() {
    wp_enqueue_style( 'custom_wp_admin_css',get_template_directory_uri() . '/src/css/panel.css');
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


function login_style() {
    wp_enqueue_style( 'custom_wp_admin_css',get_template_directory_uri() . '/src/css/login.css');
}
add_action( 'login_enqueue_scripts', 'login_style' );

/*
    Define Logged User Data
*/
global $theuser;
$theuser = wp_get_current_user();

function add_scripts(){
    //Add Global CSS
    wp_enqueue_style('bootstrap', get_template_directory_uri() . "/src/vendor/bootstrap/css/bootstrap.min.css");
    wp_enqueue_style('animate', get_template_directory_uri() . "/src/vendor/animate/animate.min.css");
    wp_enqueue_style('boxicons', get_template_directory_uri() . "/src/vendor/boxicons/css/boxicons.min.css");

    //Add jQuery
    if (!is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', get_template_directory_uri() . '/src/vendor/jquery/jquery.min.js');
        wp_enqueue_script('jquery');
    }

    wp_enqueue_script('validator-js', get_template_directory_uri() . "/src/vendor/validator/validator.js");
    wp_enqueue_script('popper-js', get_template_directory_uri() . "/src/vendor/popper/popper.min.js");
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . "/src/vendor/bootstrap/js/bootstrap.min.js");

    wp_enqueue_script('mainplugins-js', get_template_directory_uri() . "/src/js/mainplugins.js");
    wp_enqueue_script('main-js', get_template_directory_uri() . "/src/js/main.js");

    wp_enqueue_style('app', get_template_directory_uri() . "/src/css/app.css?" . rand(0,100));

    wp_enqueue_script('app-js', get_template_directory_uri() . "/src/js/front.js");

    $paramsLogin = array(
        'ajaxurl' => admin_url('admin-ajax.php'),
    );
    wp_localize_script('main-js','jsvar',$paramsLogin);

    $userid = '';
    if(is_user_logged_in()){
        global $theuser;
        $userid = $theuser->ID;
        wp_register_script('app-js', get_template_directory_uri() . "/src/js/app.js");

        $params = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'userid' => $userid,
        );
        wp_localize_script('app-js','jsvar',$params);
    }
}
add_action( 'wp_enqueue_scripts', 'add_scripts' );


/*
    Add post thumbnail
*/
add_theme_support('post-thumbnails', array('post','page'));

/*
    Custom nav menu
*/
function register_my_menus() {
    register_nav_menus(
        array(
            'menu' => 'Main menu',
        )
    );
}
add_theme_support( 'menus' );
add_action( 'init', 'register_my_menus' );


/*
	Disable Admin Bar
*/
function disable_admin_bar() {
    // if ( ! current_user_can('delete_users') ) {
    //     add_filter('show_admin_bar', '__return_false');
    // }
    add_filter('show_admin_bar', '__return_false');
}
add_action( 'after_setup_theme', 'disable_admin_bar' );


//Sanitize user in cyrillic
function non_strict_login( $username, $raw_username, $strict ) {
    if( !$strict )
        return $username;
    return sanitize_user(stripslashes($raw_username), false);
}
add_filter('sanitize_user', 'non_strict_login', 10, 3);



add_filter( 'wp_mail', 'my_wp_mail_filter' );
function my_wp_mail_filter( $args ) {
    $thebody = $GLOBALS['emailtemplate'];

    $tpl = str_replace('{{content}}', "\n". $args['message'] . "\n", $thebody);
    $tpl = str_replace("\n", '<br>', $tpl);

    //$message = strip_tags( $message );
    // Decode body
    $message = wp_specialchars_decode( $tpl, ENT_QUOTES );

    $new_wp_mail = array(
        'to'          => $args['to'],
        'subject'     => $args['subject'],
        'message'     => $message,
        'headers'     => $args['headers'],
        'attachments' => $args['attachments'],
    );

    return $new_wp_mail;
}


add_filter( 'wp_mail_content_type','mycustom_set_content_type' );
function mycustom_set_content_type() {
        return "text/html";
}

/*
	Function to facility emails notifications in WP
*/
function sendNotification($subject,$contenido,$email){
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: '.$GLOBALS['SITENAME'].' <noreply@'.$GLOBALS['DOMAIN'].'>' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    $sent = wp_mail($email, $subject, $contenido, $headers);
    if($sent){
    	return $sent;
    } else {
    	return false;
    }
}


// changing the logo link from wordpress.org to your site
function mb_login_url() {  return home_url(); }
add_filter( 'login_headerurl', 'mb_login_url' );

// changing the alt text on the logo to show your site name
function mb_login_title() { return get_option( 'blogname' ); }
add_filter( 'login_headertext', 'mb_login_title' );


/*
Footer js
*/
add_action('wp_footer', 'ga');
function ga() { ?>
<?php }
?>