<?php

add_filter("retrieve_password_message", "skeda_custom_password_reset", 99, 4);

function skeda_custom_password_reset($message, $key, $user_login, $user_data )    {
	$btn = htmlButton(network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login'),__('Reset password','skeda'));
	$message = "<p>".__('Someone has requested a password reset for the following account:','skeda-business') . "</p><b>" . sprintf(__('%s'), $user_data->user_email) . "</b><p>" . __('If this was a mistake, just ignore this email and nothing will happen. To reset your password, visit the following address:','skeda-business') . "</p>" .  '' . $btn. "\r\n" . "<p>" . __('If you have any further issues, please email us to info@skeda.ru','skeda-business') . "</p>";
	return $message;
}

