<?php

get_header();
if (have_posts()) : while (have_posts()) : the_post();

?>

<section class="fdb-block">
    <div class="container">
        <div class="row my-3">
            <div class="col-md-10 mx-auto">
                <?php the_content();?>
            </div>
        </div>
    </div>
</section>

<?php endwhile; endif;?>
<?php get_footer();?>